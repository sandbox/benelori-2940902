Metatag Custom Routes
=====================

Introduction
------------

The [Metatag Custom Routes](https://www.drupal.org/project/metatag_custom_routes)
module provides a form for adding metatags to custom routes or routes that are
otherwise not covered by
[Metatag](https://www.drupal.org/project/metatag)
and its submodules.

Requirements
------------

- [Metatag](https://www.drupal.org/project/metatag) `https://www.drupal.org/project/metatag`

Installation
------------

- Manual download from the Downloads section

OR

- Require via Composer using `composer require drupal/metatag_custom_routes`

Configuration
-------------

The configuration page is available under `Configuration > Metatag > Custom Routes`.
There, configurations can be managed for any route or path.
